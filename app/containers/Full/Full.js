import React, { Component } from 'react';
import Header from '../../components/shared/HeaderNavigation';
import Sidebar from '../../components/shared/Sidebar';
import Aside from '../../components/shared/Aside';
import Footer from '../../components/shared/Footer';

import Breadcrumbs from 'react-breadcrumbs';

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <div className="container-fluid">
              {this.props.children}
            </div>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Full;