import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';


// Containers
import Full from './containers/Full/'
import Dashboard from './views/Dashboard/'

ReactDOM.render(
  <Router>
    <Route exact path="/" name="Home" component={Full}>

    </Route>
  </Router>,
  document.getElementById('app')
);


