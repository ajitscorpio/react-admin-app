import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <a href="http://coreui.io">CoreUI</a> &copy; 2017 College Of Psychologists Of Ontario.
        
      </footer>
    )
  }
}

export default Footer;